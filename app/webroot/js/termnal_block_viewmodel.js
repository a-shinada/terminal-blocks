function TerminalBlockViewModel(){
    var _self = this;

    /* Compo */
    this._compos = ko.observableArray();

    /* 左ナビで選択されたCompoとWires */
    this._selectedCompoWires = ko.observableArray();

    /* 右ナビで選択されたWire */
    this._selectedWire = ko.observable(null);

    /* ダイアログで選択可能なBlocks */
    this._blocks = ko.observableArray();

    /* ダイアログで選択されたBlocks */
    this._selectedBlock = ko.observable(null);

    /* Blockの検索条件 */
    this._blockSearchConditionModel = new BlockSearchConditionModel();

    /* Blockの検索中: true*/
    this._isSearchBlocks =  ko.observable(false);

    _self.Map =  function(dataFromAPI){
        _self._compos .removeAll();
        $.each(dataFromAPI.compos, function(){
            var model = ko.mapping.fromJS(this);
            $.each(model.Wires(), function(){
                /* 端子台に追加されたBlocks */
                this.Blocks = ko.observableArray();
            });
            _self._compos.push(model);
        });
    }

    _self.onCompoListClick = function(selectedCompo, evt){
        _self._selectedCompoWires(selectedCompo.Wires());

        // MEMO: コンポの選択状態の制御
        $("#compo-list").find("a").removeClass("active");
        $(evt.target).closest("a").addClass("active");

        return false;
    }

    _self.onCompoWireSelected = function(selectedWire, evt){
        // MEMO: ダイアログ状態をクリア
        _self._blocks .removeAll();
        _self._selectedBlock(null);
        _self._selectedWire(selectedWire);

        $("#search-wire").modal("show");
        return false;
    }

    /*
     * Block検索ボタンクリック
     */
    _self.onBlockSearchClick = function(){
        _self._isSearchBlocks(true);
        _self._blocks .removeAll();

        $.ajax({
            type: "Get",
            url: "/terminal-block/api/v1/blocks/search.json",
            dataType: "json",
            data: $.param(_self._blockSearchConditionModel),
            success: function(data){

                _self._isSearchBlocks(false);
                $.each(data.blocks, function(){
                    var model = ko.mapping.fromJS(this);
                    _self._blocks.push(model);
                });

            } //* success end *//
        });  //* ajax end *//

        return false;
    }

    /*
     * Block検索結果クリック
     * 詳細情報を、検索ダイアログの右ペインに表示する。
     */
    _self.onBlockListClick = function(selectedBlock, evt){
        _self._selectedBlock(selectedBlock.Block);
        return false;
    }

    /**
     * Block選定ボタンクリック
     */
    _self.onBlockAddClick = function(){
        // MEMO: objectのcopyを取得する。
        var model = ko.mapping.fromJSON(ko.toJSON(_self._selectedBlock));
        _self._selectedWire().Blocks.push(model);
        return false;
    }
}

