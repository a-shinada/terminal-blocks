<?php
App::uses('AppModel', 'Model');
/**
 * Compo Model
 *
 * @property Wire $Wire
 */
class Compo extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'Compos';

      public $hasAndBelongsToMany = array(
          'Wires'=> array(
              'className' => 'Wire',
              'join_table' => 'Compo_Wires',
              'foreignKey' => 'compo_id',
              'associationForeignKey' => 'wire_id',
              'with' => 'CompoWire',
          ),
      );
}
