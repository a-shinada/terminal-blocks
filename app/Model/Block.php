<?php
App::uses('AppModel', 'Model');
/**
 * Block Model
 *
 */
class Block extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'Blocks';

	/**
	* 検索条件
	* see it : http://mawatari.jp/archives/introduction-of-cakedc-search-plugin-for-cakephp
	* 	  https://github.com/CakeDC/search
	*/
	public $actsAs = array('Search.Searchable');
	public $filterArgs = array(
	    'width' => array('type' => 'value' , 'empty' => true ),
	    'height' => array('type' => 'value' , 'empty' => true),
        'depth' => array('type' => 'value' , 'empty' => true),
        'format' => array('type' => 'like' , 'empty' => true),
	);
}
