<?php
App::uses('AppModel', 'Model');
/**
 * Wire Model
 *
 * @property CompoWireBlock $CompoWireBlock
 */
class Wire extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'Wires';
}
