<?php
App::uses('AppController', 'Controller');

class TerminalBlockController extends AppController {
    public $components = array('RequestHandler', 'Search.Prg', 'Paginator');
    public $presetVars = true;

    public $paginate = array(
        'limit' => 25,
        'order' => array(
            'Post.title' => 'asc'
        )
    );

    public function index() {
        $this->loadModel('Compo');
        $this->Compo->recursive = 3;
        $compos = $this->Compo->find('all');
        $this->set(array(
            'compos' => $compos,
            '_serialize' => array('compos')
        ));
    }

    public function get_blocks() {
        $this->loadModel('CompoWire');

        $this->Prg->commonProcess();
        $this->paginate = array(
            'conditions' => $this->CompoWire->parseCriteria($this->passedArgs),
        );

        $this->set(array(
            'blocks' => $this->paginate(),
            '_serialize' => array('blocks')
        ));
    }

    public function search_blocks() {
        $this->loadModel('Block');
        $blocks = $this->Block->find('all');
        $this->set(array(
            'blocks' => $blocks,
            '_serialize' => array('blocks')
        ));
    }
}
