<?php
App::uses('AppController', 'Controller');

class BlocksController extends AppController {
    public $components = array( 'Search.Prg' , 'RequestHandler' );
    //public $presetVars = true;

    public function search() {

        $this->Prg->commonProcess();
        $this->paginate = array(
            'conditions' => $this->Block->parseCriteria($this->passedArgs),
        );

        $blocks = $this->paginate();
        $this->set(array(
            'blocks' => $blocks,
            '_serialize' => array('blocks')
        ));
    }
}
