<?php
App::uses('AppController', 'Controller');

class ComposController extends AppController {
    public $components = array('RequestHandler');

    public function index_dummy() {
        $this->loadModel('Compo');
        $this->Compo->recursive = 2;
        $compos = $this->Compo->find('all');
        $this->set(array(
            'compos' => $compos,
            '_serialize' => array('compos')
        ));
    }

    public function index() {
        $this->loadModel('Compo');
        $model = array(
            array('id' => 1, 
                  'format' => 'K8AK-□□A', 
                  'img_url' => 'http://www.fa.omron.co.jp/Images/l_154-14-136803-78x78.jpg',
                  'url' => 'http://www.ehonnavi.net',
                  'Wires' => array(
                      array('id' => 1, 'format' => 'AWG #20(0.518mm2)', 'status' => 'not_selected', 'Blocks' => array()),
                      array('id' => 2, 'format' => 'AWG #15(1.65mm2)', 'status' => 'not_selected', 'Blocks' => array()),
                      array('id' => 3, 'format' => 'AWG #22(0.518mm2)', 'status' => 'not_selected', 'Blocks' => array()),
                  )
              ),
            array('id' => 2, 
                  'format' => 'SUP-COMP', 
                  'img_url' => 'http://www.fa.omron.co.jp/Images/l_1755-14-119082-78x78.jpg',
                  'url' => 'http://www.ehonnavi.net',
                  'Wires' => array(
                      array('id' => 3, 'format' => 'AWG #22(0.518mm2)', 'status' => 'not_selected', 'Blocks' => array()),
                      array('id' => 4, 'format' => 'AWG #18(1.65mm2)', 'status' => 'not_selected', 'Blocks' => array()),
                  )
              ),
              array('id' => 3, 
                  'format' => 'AKSH', 
                  'img_url' => 'http://www.fa.omron.co.jp/Images/l_1756-14-119083-78x78.jpg',
                  'url' => 'http://www.ehonnavi.net',
                  'Wires' => array(
                      array('id' => 3, 'format' => 'AWG #22(0.518mm2)', 'status' => 'not_selected', 'Blocks' => array()),
                      array('id' => 4, 'format' => 'AWG #18(1.65mm2)', 'status' => 'not_selected', 'Blocks' => array()),
                  )
              ),
              array('id' => 4, 
                  'format' => 'AKSH-2', 
                  'img_url' => 'http://www.fa.omron.co.jp/Images/l_1755-14-119082-78x78.jpg',
                  'url' => 'http://www.ehonnavi.net',
                  'Wires' => array(
                      array('id' => 1, 'format' => 'AWG #22(0.518mm2)', 'status' => 'not_selected', 'Blocks' => array()),
                      array('id' => 4, 'format' => 'AWG #18(1.65mm2)', 'status' => 'not_selected', 'Blocks' => array()),
                  )
              ),

        );
        $this->set(array(
            'compos' => $model,
            '_serialize' => array('compos')
        ));
    }

}
