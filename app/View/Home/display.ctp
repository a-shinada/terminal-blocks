<?php
    echo $this->Html->css(array('terminal_block.css', 'bootstrap.min.css'), false, array('inline'=>false));
    echo $this->Html->script(array('jquery-1.11.2.min.js', 'knockout-3.3.0.js', 'knockout.mapping-latest.js', 'termnal_block_viewmodel.js', 'block_search_condition_model.js', 'bootstrap.min.js'), array('inline'=>false));
?>
<script type="text/javascript">
    $(function($) {

        var viewModel = new TerminalBlockViewModel();
        ko.applyBindings(viewModel, $("#knockout-wrap")[0]);
        $.ajax({
            type: "Get",
            url: "/terminal-block/api/v1/compos/index.json",
            success: function(data){
                viewModel.Map(data);
            }
        });
    });
</script>
<div id="knockout-wrap" class="container">

    <div id="termnal-main-wrap" class="row" >

        <!-- コンポリスト -->
        <div class="col-sm-6 col-md-6">
            <div id="compo-list" class="list-group" data-bind="template: { name: 'composTemplate', foreach: _compos}">
            </div>
            <script type="text/html" id="composTemplate">
                <a href="#" class="list-group-item" data-bind="click: $root.onCompoListClick">

                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-3">
                            <span class="label label-danger">未選定</span>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-right">
                            <h4 data-bind="text: format"></h4>
                        </div>
                    </div>
                    <div class="row list-inner" >
                        <div class="col-xs-3 col-sm-3 col-md-3 col-left">
                            <img  class="img-responsive" data-bind="attr: { src: img_url }">
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <button class="btn btn-default" data-bind="attr: { data: url }">詳細ページへ</button>
                        </div>
                    </div>
                </a>
            </script>
        </div><!-- / コンポリスト -->

        <div class="col-sm-6 col-md-6">
            <table class="table">
                <tr>
                    <th>#</th>
                    <th>適応電線</th>
                    <th>状態</th>
                </tr>
                <!-- ko foreach: _selectedCompoWires -->
                <tr>
                    <td data-bind="text: $index() + 1"></td>
                    <td><a href="#" data-bind="text: format, click: $root.onCompoWireSelected"></a></td>
                    <td>not selected</td>
                </tr>
                <!-- /ko -->
            </table>
        </div>
    </div>

    <div class="footer navbar-fixed-bottom">
        <!-- 選択済みブロック表示エリア -->
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <label for="" class="form-label">外形寸法</label>
                <span>横：</span><span>縦：</span><span>奥行き：</span>
            </div>
        </div>

        <div class="row">
                <!-- ko if: _selectedWire() != null -->
                <ul  id="terminal-list" class="nav pull-left nav-pills" data-bind="template:{ name:  'added-blocks', foreach: _selectedWire().Blocks()}"></ul>
                <!-- /ko -->
                <script type="text/html" id="added-blocks">
                    <li>
                        <div class="block">
                            <img data-bind="attr: { src: block_img_url, alt: format }"/>
                        </div>
                    </li>
                </script>
        </div>
        <!-- 選択済みブロック表示エリア -->
    </div> <!-- /.footer -->

    <!-- modal -->
    <div id="search-wire" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4>適応電線フィルタ</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label for="txtWidth">幅：<input id="txtWidth" type="text" data-bind="value: _blockSearchConditionModel.width"></label>
                        <label for="txtFormat">型式：<input id="txtFormat" type="text" data-bind="value: _blockSearchConditionModel.format"></label>
                        <label for="txtHeight">高：<input id="txtHeight" type="text" data-bind="value: _blockSearchConditionModel.height"></label>
                        <label for="txtDepth">奥：<input id="txtDepth" type="text" data-bind="value: _blockSearchConditionModel.depth"></label>
                    </div>
                    <div class="row text-center">
                        <button data-bind="click: onBlockSearchClick" type="button" class="btn btn-default">Search</button>
                    </div>
                    <div class="row text-center load-search-blocks" data-bind="visible:  _isSearchBlocks" >
                    </div>
                    <div class="row" style="margin: 20px 0 0 0;">
                        <div class="col-md-6">
                            <div class="list-group" data-bind="template:{ name:  'searched-blocks', foreach: _blocks}"></div>
                            <script type="text/html" id="searched-blocks">
                                <a href="#" class="list-group-item" data-bind="click: $root.onBlockListClick">
                                    <div class="media-body">
                                        <h4 class="media-heading" data-bind="text: Block.format"></h4>
                                    </div>
                                </a>
                            </script>
                        </div>

                        <!-- ko if: _selectedBlock() != null -->
                        <div class="col-md-6 block-detail">
                            <div class="row block-detail-img">
                                <img data-bind="attr: { src: _selectedBlock().img_url }">
                            </div>
                            <div class="row block-detail-description" data-bind="html: _selectedBlock().detail_html">
                            </div>
                            <div class="row text-right block-detail-selectbtn-wrap">
                                <button class="btn btn-success" data-bind="{ click: onBlockAddClick }">選定</button>
                            </div>
                        </div>
                        <!-- /ko -->

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /#search-wire -->
    <!-- modal end -->

</div><!-- /#nockout-wrap -->
