<?php
/**
 * CompoWireBlockFixture
 *
 */
class CompoWireBlockFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'CompoWire_Blocks';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'wire_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'block_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'seq' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'wire_id' => 1,
			'block_id' => 1,
			'seq' => 1,
			'created' => '2015-11-27 01:06:34',
			'modified' => '2015-11-27 01:06:34'
		),
	);

}
