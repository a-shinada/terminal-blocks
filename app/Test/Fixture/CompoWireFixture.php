<?php
/**
 * CompoWireFixture
 *
 */
class CompoWireFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'Compo_Wires';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'compo_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'wire_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'compo_id' => 1,
			'wire_id' => 1,
			'created' => '2015-11-26 08:10:12',
			'modified' => '2015-11-26 08:10:12'
		),
	);

}
