<?php
App::uses('CompoWireBlock', 'Model');

/**
 * CompoWireBlock Test Case
 *
 */
class CompoWireBlockTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.compo_wire_block'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CompoWireBlock = ClassRegistry::init('CompoWireBlock');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CompoWireBlock);

		parent::tearDown();
	}

}
