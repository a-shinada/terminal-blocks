<?php
App::uses('Wire', 'Model');

/**
 * Wire Test Case
 *
 */
class WireTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.wire',
		'app.block'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Wire = ClassRegistry::init('Wire');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Wire);

		parent::tearDown();
	}

}
