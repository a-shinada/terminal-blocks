<?php
App::uses('CompoWire', 'Model');

/**
 * CompoWire Test Case
 *
 */
class CompoWireTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.compo_wire'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CompoWire = ClassRegistry::init('CompoWire');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CompoWire);

		parent::tearDown();
	}

}
