<?php
App::uses('BlocksCompoWire', 'Model');

/**
 * BlocksCompoWire Test Case
 *
 */
class BlocksCompoWireTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.blocks_compo_wire'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->BlocksCompoWire = ClassRegistry::init('BlocksCompoWire');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->BlocksCompoWire);

		parent::tearDown();
	}

}
