<?php
App::uses('CompoWiresMap', 'Model');

/**
 * CompoWiresMap Test Case
 *
 */
class CompoWiresMapTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.compo_wires_map'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CompoWiresMap = ClassRegistry::init('CompoWiresMap');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CompoWiresMap);

		parent::tearDown();
	}

}
